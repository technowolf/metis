# Metis - Open Source Coming Soon Template

Metis is a open source coming soon template for your website or a starter project.
Metis is being developed and curently in WIP stage

Check out the **[Demo](https://daksh7011.com/metis)**.

Demo is being updated with each commit in **[beta](https://gitlab.com/daksh7011/metis/tree/beta)** branch

## Features (WIP)
+ Bootstrap 4 Compatible ✅
+ Responsive Design ✅
+ Countdown (WIP) ❌
+ Preloader ✅
+ Slider (WIP) ❌
+ Classic and Overlay Navigation(WIP) ❌
+ Back to top (WIP) ❌
+ Light and Dark Style (WIP) ❌
+ Multiple Button Styles (WIP) ❌
+ W3C Valid HTML ✅
+ Lightbox Gallery for Portfolio (WIP) ❌
+ Tons of icons – Font Awesome 5 integrated ✅

And much more.

Any Contribution is welcomed. Please open a issue if you find bugs or wanna contribute by any means.

**Opensource your work, Knowledge is *free*.**